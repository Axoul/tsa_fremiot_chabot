function [x1,x2,x3,Az,Bz] = syntheseSA(N,B,m3,sigma3)
  fs = 1000;
  x1 = randn(1,N); %Variable aléatoire N fois
  [Bz,Az] = butter(8,B/(fs/2)); %Création du filtre passe bas de butterworth d'ordre 8 avec la fc à B
  x2 = filter(Bz,Az,x1); %Application du signal sur le filtre
  x2p = (x2 - mean(x2))./std(x2); %Centrage et réduction de x2 pour l'appliquer à x3
  x3 = x2p.*sigma3+m3; 
end