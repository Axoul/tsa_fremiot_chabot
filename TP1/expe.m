close all;
clear variables;

Fs = 1000;
Ts = 1/Fs;
N = 1000;
B = 100;
m3 = 6;
sigma3 = 4;
[x1,x2,x3,Az,Bz] = syntheseSA(N,B,m3,sigma3); %Création des 3 signaux aléatoires
[N1,C1] = histogramme(x1); %Calcul du premier histogramme
[N2,C2] = histogramme(x2); %Calcul du second histogramme
[N3,C3] = histogramme(x3); %Calcul du troisième histogramme
k = 0:N-1; %Creation des indices de N de long
figure;
subplot(2,4,1);
plot(k/Fs,x1); %Affichage des signaux en fréquence réduite
title('Signal X1');
xlabel('t');
ylabel('Amplitude');
subplot(2,4,2);
plot(k/Fs,x2);
title('Signal X2');
xlabel('t');
ylabel('Amplitude');
subplot(2,4,3);
plot(k/Fs,x3);
title('Signal X2');
xlabel('t');
ylabel('Amplitude');
[h,f]=freqz(Bz,Az,N,Fs); %Création du filtre de butterworth en fonction de ses poles
subplot(2,4,4);
plot(f,abs(h)); %Affichage fréquentiel du filtre
title('Module du gain complexe du filtre PB');
xlabel('Fréquence en Hz');
ylabel('Gain');

xx1 = min(C1):0.1:max(C1); %Création vecteur "fin" des abscisse pour l'affichage des courbes théoriques 
norme1=(1/sqrt(2*pi))*exp(-((xx1-0).^2)/2); %Création courbe théorique du premier signal (centré réduit)
subplot(2,4,5);
bar(C1,N1);
hold on;
plot(xx1, norme1, '-r');
hold off;
title('Densité de probabilité de X1');
xlabel('Classes');
ylabel('Densité de probabilité');
xx2 = min(C2):0.1:max(C2);
norme2=(1/((sqrt(2*B/Fs))*sqrt(2*pi)))*exp(-((xx2-0).^2)/(2*(sqrt(2*B/Fs)).^2)); %Création courbe théorique du second signal (centré)
subplot(2,4,6);
bar(C2,N2);
hold on;
plot(xx2, norme2, '-r');
hold off;
title('Densité de probabilité de X2');
xlabel('Classes');
ylabel('Densité de probabilité');
xx3 = min(C3):0.1:max(C3);
norme3=(1/(sigma3*sqrt(2*pi)))*exp(-((xx3-m3).^2)./(2*sigma3.^2)); %Création courbe théorique du troisième signal
subplot(2,4,7);
bar(C3,N3);
hold on;
plot(xx3, norme3, '-r');
hold off;
title('Densité de probabilité de X3');
xlabel('Classes');
ylabel('Densité de probabilité');

mean11 = mean(x1); %Premier calcul de moyenne
mean12 = mean(x2);
mean13 = mean(x3);

mean21 = C1(find(N1==max(N1))); %Calcul de la moyenne avec l'aide de l'histogramme
mean22 = C2(find(N2==max(N2)));
mean23 = C3(find(N3==max(N3)));

ecart11 = std(x1);
ecart12 = std(x2);
ecart13 = std(x3);

ecart21 = 1/(max(N1)*sqrt(2*pi));
ecart22 = 1/(max(N2)*sqrt(2*pi));
ecart23 = 1/(max(N3)*sqrt(2*pi));

ecart31 = (max(x1) - min(x1))/6;
ecart32 = (max(x2) - min(x2))/6;
ecart33 = (max(x3) - min(x3))/6;

moit1 = find(N1>=(max(N1)/2));
moit2 = find(N2>=(max(N2)/2));
moit3 = find(N3>=(max(N3)/2));
ecart41 = (C1(moit1(end))-C1(moit1(1)))/2.35;
ecart42 = (C2(moit2(end))-C2(moit2(1)))/2.35;
ecart43 = (C3(moit3(end))-C3(moit3(1)))/2.35;