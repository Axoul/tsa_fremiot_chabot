close all;
clear variables;

Fs = 1000;
Ts = 1/Fs;
M = 20;
B = 100;
m3 = 6;
sigma3 = 4;

i = 1;

figure;
for N=[2^4 2^5 2^6 2^7 2^8 2^9 2^10 2^11]
    [x1,x2,x3,Az,Bz] = syntheseSA(N,B,m3,sigma3); %Création des 3 signaux aléatoires
    [N1,C1] = histogramme(x1,M); %Calcul du premier histogramme
    subplot(2,4,i);
    stem(C1,N1);
    hold on;
    xx = min(C1):0.1:max(C1);
    norme=(1/sqrt(2*pi))*exp(-((xx-0).^2)/2);
    plot(xx,norme);
    title(['Densité de probabilité pour N = ',num2str(N)]);
    xlabel('Classes');
    ylabel('Densité de probabilité');
    i = i +1;
end