function [ddpe,Ci] = histogramme(x,M)
    N = length(x);
    if nargin == 2 %Si M est donné
        deltaX = (max(x)-min(x))/M; %DeltaX imposé
    else    
        sigma = std(x);
        deltaX = 3.49*sigma*N^(-1/3); %DeltaX optimal
        M = round((max(x)-min(x))/deltaX);
    end    
    [Ni,Ci] = hist(x,M);
    ddpe = Ni./(N*deltaX); %Densité de probabilité normalisée
%     figure;
%     bar(Ci,ddpe); %Affichage de la ddpe par rapport aux classes
%     if nargin == 2
%         xlabel(['Pour \DeltaX (imposé) = ',num2str(deltaX)]);
%     else
%         xlabel(['Pour \DeltaX (optimal) = ',num2str(deltaX)]);
%     end
%     ylabel('Densité de probabilité');
%     title('Densité de probabilité estimée en fonction d un bruit blanc');
end