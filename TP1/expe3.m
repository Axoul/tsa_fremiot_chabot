close all;
clear variables;

Fs = 1000;
Ts = 1/Fs;
N = 1000;
B = 100;
m3 = 6;
sigma3 = 4;
[x1,x2,x3,Az,Bz] = syntheseSA(N,B,m3,sigma3);
i = 1;

figure;
for M=[2 10 20 50 100 250 500 700 1000]
    [N1,C1] = histogramme(x1,M);
    subplot(2,5,i);
    stem(C1,N1);
    hold on;
    xx = min(C1):0.1:max(C1);
    norme=(1/sqrt(2*pi))*exp(-((xx-0).^2)/2);
    plot(xx,norme);
    title(['Pour M = ',num2str(M)]);
    xlabel('Classes');
    ylabel('Densité de probabilité');
    i = i +1;
end
N = length(x1);
sigma = std(x1);
deltaX = 3.49*sigma*N^(-1/3);
M = round((max(x1)-min(x1))/deltaX);
[N1,C1] = histogramme(x1,M);
subplot(2,5,10);
stem(C1,N1);
hold on;
xx = min(C1):0.1:max(C1);
norme=(1/sqrt(2*pi))*exp(-((xx-0).^2)/2);
plot(xx,norme);
title(['Pour M = ',num2str(M),' (optimisé)']);
xlabel('Classes');
ylabel('Densité de probabilité');