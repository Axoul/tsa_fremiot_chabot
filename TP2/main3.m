close all;
clear variables;

x = genbrfil;
N = 10000;
Nom_fenetre = 'blackman';
M = 60;
NOVERLAP = 50;
NFFT = 2^14;
estimateur_welch(x,N,Nom_fenetre,M,NOVERLAP,NFFT);