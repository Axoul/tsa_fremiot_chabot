function estimateur_spectral_moyenne(bruit,N,M,NFFT)
    [PSD, F]=pwelch(bruit,rectwin(M),0,NFFT,1,'twosided');
    [Gth,Gbiais,f]=sptheo(M,'moyenne',rectwin(M));
    figure;
    plot(F,10*log10(PSD),'r', f, Gth, 'b', f, Gbiais, 'g');
    axis([0, max(f), -50,10]);
    title(['Estimation spectrale de moyenne avec FFT sur ', num2str(NFFT), ' points et un signal de ', num2str(N), ' séquences de taille ', num2str(M), ' échantillons']);
    xlabel('Fréquence réduite');
    ylabel('Amplitude en dB');
end