function estimateur_welch(bruit,N,Nom_fenetre,M,NOVERLAP,NFFT)
    [Gth,Gbiais,f] = sptheo(M,'welch',Nom_fenetre);
    eval(['WIN=',Nom_fenetre,'(M)']);
    [PSD, F] = pwelch(bruit,WIN,NOVERLAP,NFFT,1,'twosided');
    figure;
    plot(F,PSD,'r',f,Gth,'b',f,Gbiais,'y');
    axis([0,max(f),-50,10]);
    title(['Estimation spectrale de Welch avec FFT sur ',num2str(NFFT),' points et un signal de ', num2str(N),' sequences de taille ',num2str(M),' echantillons avec une fenetre de type ', Nom_fenetre, ' et un recouvrement de ',num2str(NOVERLAP)]);
    xlabel('frequence reduite');
    ylabel('amplitude en dB');
end