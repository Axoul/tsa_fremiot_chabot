function estimateur_spectral(bruit,nd,nf,NFFT)
    N = nf-nd+1;
    f1 = [0:1/NFFT:(1-1/NFFT)];
    gammachapeau1 = (abs(fft(bruit,NFFT)).^2)/N;
    [Gth,Gbiais,f] = sptheo(N,'simple');
    figure;
    %hold on;
    aff = plot(f1,10*log10(gammachapeau1),'r',f,Gth,'b',f,Gbiais,'g');
    aff(2).LineWidth = 4;
    aff(3).LineWidth = 4;
    axis([0 0.5 -50 10]);
    title(['Affichage de de la mesure, de la DSPM et du biais sur un signal de ',num2str(N),' points, avec NFFT = ',num2str(NFFT)]);
    legend('Valeur Estime','DSPM','Biais');
end