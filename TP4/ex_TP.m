close all;
clear variables;

figure(1);
[s, Fs] = audioread('ProtestMonoBruit.wav');

%sound (s,Fs); %Peremt l'ecoute de la musique 
M=length(s);
temps=0 : 1/Fs : (M-1)/Fs ;  %Vecteur temps

plot(temps,s);
title('Signal dorigine ');
xlabel('t');
ylabel('Amplitude');


figure(2);

K=200;
[R,lags] = xcorr(s(60*Fs:70*Fs),K,'biased');
plot(lags, R);
title('Représentation de lautocorrelation permettant de savoir le M optimal');
xlabel('M');
ylabel('Autocorrelation');

figure(3);
M=20;
Ar= zeros(21,1);        % Matrice 1 colonne que des zéros
Ar(1,1)=1;    % On place 1 sur la premiere ligne de la matrice  
T= toeplitz (R(201:221)); % On créer la mattrice Toeplitz des autocorr 
% on prend un tervalles de 20 et K=200 donc pr 200 < k < 220
Inver=pinv(T);  % on crée la matrice psoeudo-inverse
Phi=Inver*Ar ;   % on multiplie par notre matrice (1,0,0...) 

Phi0=Inver(1,1);  % On crée le Phi0 (prépa TD)
sigma_carre= 1 / Phi0 ;   % On obtient le signa carré


h= (-1)*Phi(2:end)*sigma_carre ; % (equation prépa TD)

figure(3)
stem([1:20], h);
title('sequence h(k)');
xlabel('k');

N=length(s);
prediction = zeros(1,length(s));

for i=1:N
    for j= 1:M 
        if (i-j)>0 
        prediction(i)= prediction(i)+h(j)*s(i-j);
        end
   end
end


figure(4)
subplot(211);
plot(temps,prediction,'r',temps,s,'b');
title('Représentation du signal prédit');
xlabel('t');
ylabel('Amplitude');

Erreur=abs(prediction-s');
subplot(212);
plot(temps,Erreur)

seuil = 0.035;


for y=1:N
    if Erreur(y)>= seuil
        prediction(y) = median(s(y-10:y+10));
    end
end


epsilon = abs(s'-prediction);
figure(5)
plot(temps, s,'b', temps, prediction,'r'); %on a donc largement reduit le bruit
title('Signal prédit auquel nous appliquons la fonction médiane');
xlabel('t');
ylabel('Amplitude');

%sound(prediction, Fs);

prediction_causale=zeros(N,1);
prediction_acausale=zeros(N,1);
predic_total = zeros(N,1);

for i=1:N
    for j= 1:M 
        if (i-j)>0 
        prediction_causale(i)= prediction_causale(i)+h(j)*s(i-j);
        end
   end
end

for i=1:N
    for j= 1:M 
        if (i+j)<N 
        prediction_acausale(i)= prediction_acausale(i)+h(j)*s(i+j);
        end
   end
end

length(s)


for n=1:1:N 
    for k=1:1:M
        predic_total(n)= ( prediction_causale(n) + prediction_acausale(n))/2;
    end
end 

epsilon_causale = (s-prediction_causale);
epsilon_acausale = (s-prediction_acausale);
epsilon_total = (s-predic_total);

seuil=0.035;

for y=1:N
    if epsilon_causale(y)> seuil
        prediction_causale(y) = median(s(y-10:y+10));
    end
    
    if epsilon_total(y)> seuil
        predic_total(y) = median(s(y-10:y+10));
    end
    
    
end


figure(6);
hold on;
plot(temps,s,'g',temps,predic_total,'r')
title('Signal dorigine et le signal filtré en fonction du temps');
xlabel('t');
ylabel('Amplitude');

