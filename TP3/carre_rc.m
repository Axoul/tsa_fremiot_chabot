close all;
clear variables;

Xp = struct('sigma',sqrt(5),'Fs',500,'B',160,'T',100);
figure(1);
[X,Xp] = CGN(Xp);
Fp = struct('Fs',500,'F0',100,'Dnu',16,'order',6,'class','BP Filter');
figure(2);
[Y,Fp] = BPF(X,Fp);

Dv = 16;
for DvRC = [2 20 100]
   RC = DvRC/Dv;
   Z = SquareSig(Y);
   RCFp = struct('Fs',500,'RC',RC);
   figure;
   [W,RCFp] = RCF(Z,RCFp);
   moyenne = mean(W.data);
   ecart = std(W.data)^2;
   kur = kurtosis(W.data);
   
   debut = find(W.time<5*RC,1,'last');
   moyenne_corr = mean(W.data(debut:end));
   ecart_corr = std(W.data(debut:end))^2;
   kur_corr = kurtosis(W.data(debut:end));
   
   fprintf('DvRC = %d | RC = %d | Moyenne = %d | Ecart Type = %d | Kurtosis = %d\n',DvRC,RC,moyenne,ecart,kur);
   fprintf('Corrigé | DvRC = %d | RC = %d | Moyenne = %d | Ecart Type = %d | Kurtosis = %d\n',DvRC,RC,moyenne_corr,ecart_corr,kur_corr);
end