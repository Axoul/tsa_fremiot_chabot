close all;
clear variables;

Xp = struct('sigma',sqrt(5),'Fs',500,'B',160,'T',100);
[X,Xp] = CGN(Xp);
moyenne = mean(X.data);
variance = std(X.data)^2;
fprintf('La moyenne de B(t) est de %d\nLa variance est de %d\n',moyenne,variance);