close all;
clear variables;

Xp = struct('sigma',sqrt(5),'Fs',500,'B',160,'T',100);
figure(1);
[X,Xp] = CGN(Xp);
Fp = struct('Fs',500,'F0',100,'Dnu',16,'order',6,'class','BP Filter');
figure(2);
[Y,Fp] = BPF(X,Fp);
moyenne = mean(Y.data);
variance = std(Y.data)^2;
fprintf('La moyenne de Y_b(t) est de %d\nLa variance est de %d\n',moyenne,variance);