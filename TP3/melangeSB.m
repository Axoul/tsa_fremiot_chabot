close all;
clear variables;

T = 100;
Fs = 500;
F0 = 100;
B = 160;
Dv = 16;
Amp = 1;
Fc = 100;

Xp = struct('sigma',sqrt(5),'Fs',Fs,'B',B,'T',T);
Fp = struct('Fs',Fs,'F0',F0,'Dnu',Dv,'order',6,'class','BP Filter');
Sp = struct('Fs',Fs,'A',Amp,'Fc',Fc,'FM',0,'Phi',0,'T',T,'W',[]); %Pas de modulation

figure(1);
[S,Sp,M] = OOK(Sp); %Génération de S(t) Signal

figure(2);
[B,Xp] = CGN(Xp); %Génération de B(t) Bruit

[X] = AddSig(S,B); %Addition des signaux
figure(3);
[Y,Fp] = BPF(X,Fp); %Filtrage Passe Bande de X(t)

for DvRC = [2 20 100]
   RC = DvRC/Dv;
   Z = SquareSig(Y);
   RCFp = struct('Fs',Fs,'RC',RC);
   figure;
   [W,RCFp] = RCF(Z,RCFp);
end